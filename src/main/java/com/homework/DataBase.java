package com.homework;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.Properties;

public class DataBase {

    public static Session session;
    public static Properties properties = new Properties();

    static {
        try {
            properties.setProperty("hibernate.connection.url", "jdbc:mysql://localhost:3306/homework");
            properties.setProperty("hibernate.connection.username", "root");
            properties.setProperty("hibernate.connection.password", "password");
            properties.setProperty("hibernate.connection.dialect", "org.hibernate.dialect.MySQL8Dialect");
            properties.setProperty("hibernate.hbm2ddl.auto", "update");
            properties.setProperty("show_sql", "true");

            SessionFactory factory = new Configuration()
                    .addAnnotatedClass(com.homework.CustomerPojo.class)
                    .addProperties(properties).buildSessionFactory();
            session = factory.openSession();
        } catch (Exception e) {
            System.out.println("Exception");
            e.printStackTrace();
            throw e;
        }
    }
}

package com.homework;

import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.hibernate.Session;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.hamcrest.Matchers.equalTo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class ApplicationTests {

    private RequestSpecification request;
    private Integer id;
    private Session session = DataBase.session;
    CustomerPojo customerToCreate = new CustomerPojo();
    private String schemaJson = "" +
            "{\"properties\": {" +
            "\"name\": {" +
            "\"type\": \"string\"}, " +
            "\"age\" : {" +
            "\"type\" : \"string\"}," +
            "\"sum\" : {" +
            "\"type\" :  \"number\"}," +
            "\"id\" : {" +
            "\"type\" : \"integer\", \"minimum\" : 1} " +
            " }, \"required\" : [\"name\", \"age\", \"sum\", \"id\"]" +
            "}";

    private String schemaSwagger = "{\n" +
            "  \"properties\": {\n" +
            "    \"paths\": {\n" +
            "      \"type\": \"object\",\n" +
            "      \"additionalProperties\": true,\n" +
            "      \"properties\": {\n" +
            "        \"/error\": { \"type\": \"object\" },\n" +
            "        \"/customer\": {\"type\": \"object\",\n" +
            "          \"properties\": { \"get\": {\"type\": \"object\"},\n" +
            "            \"post\": {\"type\": \"object\" }},\n" +
            "          \"required\": [ \"get\", \"post\"]},\n" +
            "        \"/customer/{id}\": {\"type\": \"object\"} },\n" +
            "      \"required\": [\"/error\",\"/customer\", \"/customer/{id}\" ]\n" +
            "    }  }}";


    @LocalServerPort
    private Integer port;

    @BeforeEach
    public void createRestRequest() {
        request = RestAssured.given();
        RestAssured.baseURI = "http://localhost:" + port;
        request.contentType("application/json");
        CustomerPojo customerToCreate = new CustomerPojo();
    }

    @Test
    void creatCustomerTest() {
        customerToCreate.setName("nameForCreateTest");
        customerToCreate.setAge("33");
        customerToCreate.setSum(22222);
        String response = request
                .body(customerToCreate)
                .post("/customer")
                .then()
                .statusCode(201)
                .body(matchesJsonSchema(schemaJson))
                .extract()
                .asString();
        Integer id = Integer.valueOf(response);
        Assertions.assertTrue(id > 0);
        CustomerPojo customerDB = session.get(CustomerPojo.class, id);
        Assertions.assertEquals(customerDB.getName(), customerToCreate.getName());
        Assertions.assertEquals(customerDB.getAge(), customerToCreate.getAge());
        Assertions.assertEquals(customerDB.getSum(), customerToCreate.getSum());
    }

    @Test
    public void getCustomerTest() {
        customerToCreate.setName("nameForGetTest");
        customerToCreate.setAge("56");
        customerToCreate.setSum(1234567890);
        String response = request
                .body(customerToCreate)
                .post("/customer")
                .asString();
        Integer id = Integer.valueOf(response);
        CustomerPojo customerDB = session.get(CustomerPojo.class, id);
        ValidatableResponse responseFromGet = request.contentType("application/json")
                .get("/customer?id=" + id)
                .then()
                .statusCode(200)
                .body(matchesJsonSchema(schemaJson))
                .body("name", equalTo(customerDB.getName()))
                .body("age", equalTo(customerDB.getAge()))
                .body("sum", equalTo(customerDB.getSum()))
                .body("id", equalTo(customerDB.getId()));
    }

    @Test
    public void successDeleteCustomerTest() {
        customerToCreate.setName("nameForDeleteTest");
        customerToCreate.setAge("99");
        customerToCreate.setSum(987654321);
        String response = request
                .body(customerToCreate)
                .post("/customer")
                .asString();
        Integer id = Integer.valueOf(response);
        CustomerPojo customerDB = session.get(CustomerPojo.class, id);
        ValidatableResponse responseDelete = request.contentType("application/json")
                .delete("/customer/" + id)
                .then()
                .statusCode(200)
                .body(matchesJsonSchema(schemaJson))
                .body("name", equalTo(customerDB.getName()))
                .body("age", equalTo(customerDB.getAge()))
                .body("sum", equalTo(customerDB.getSum()))
                .body("id", equalTo(customerDB.getId()));
        CustomerPojo customerDBAfterDelete = session.get(CustomerPojo.class, id);
        Assertions.assertNull(customerDBAfterDelete);
    }

    @Test
    public void swaggerTesting() {
        request.contentType("application/json")
                .get("/v2/api-docs")
                .then()
                .assertThat()
                .body(matchesJsonSchema(schemaSwagger));
    }
}

